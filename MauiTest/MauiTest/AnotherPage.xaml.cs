namespace MauiTest;

public partial class AnotherPage : ContentPage
{
	public AnotherPage()
	{
		InitializeComponent();
	}

    private void Button_Clicked(object sender, EventArgs e)
    {
		//Navigation.PopAsync(new ContentPageDemo());
		//No es neceasario referenciar la pagina desapilando
		Navigation.PopAsync();
	}
}