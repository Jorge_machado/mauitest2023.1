using Sales.Mobile.BindingDemo;

namespace MauiTest.BindingDemo;

public partial class BindigPage : ContentPage
{
    Person _person = new();
	public BindigPage()
	{
		InitializeComponent();

        _person = new Person
        {
            Address = "Calle Luna Calle Sooolllll",
            Name = "Jorge Machado",
            Phone = "301 771 2455"
        };

        BindingContext = _person;
    }

    private void btnOk_Clicked(object sender, EventArgs e)
    {
        #region Forma 1 Binding
        //var person = new Person
        //{
        //    Address = "Calle Luna Calle Sol",
        //    Name = "Jorge Machado",
        //    Phone = "301 771 24 55"
        //};

        //var personBinding = new Binding();
        //personBinding.Source = person;
        //personBinding.Path = "Name";

        //lblName.SetBinding(Label.TextProperty, personBinding);
        #endregion

        #region Forma 3 Binding
        //var person = new Person
        //{
        //    Address = "Calle Luna Calle Sol",
        //    Name = "Jorge Machado",
        //    Phone = "301 771 24 55"
        //};

        //lblName.BindingContext = person;
        //lblName.SetBinding(Label.TextProperty, "Name");
        #endregion

        #region Forma 4 Binding....la mejor forma de bindear
        //var person = new Person
        //{
        //    Address = "Calle Luna Calle Sol",
        //    Name = "Jorge Machado",
        //    Phone = "301 771 24 55"
        //};

        //BindingContext = person;
        #endregion

        _person.Name = "Ledys";
        _person.Phone = "322 300 1232";
        _person.Address = "Avenida Siempre Viva";


    }
}