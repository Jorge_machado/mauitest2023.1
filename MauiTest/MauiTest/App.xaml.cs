﻿using MauiTest.BindingDemo;

namespace MauiTest;

public partial class App : Application
{
	public App()
	{
		InitializeComponent();

        //MainPage = new NavigationPage (new ContentPageDemo());

        //var navPage = new NavigationPage(new ContentPageDemo());
        //navPage.BarBackgroundColor = Colors.Chocolate;
        //navPage.BarTextColor = Colors.White;
        //navPage.Title = "Demo MAUI";
        //MainPage = navPage;

        //MainPage = new NavigationPage(new FlyoutPageDemo());
        //MainPage = new NavigationPage(new TabbedPageDemo());
        //MainPage = new NavigationPage(new PresentationControlsPageDemo());
        //MainPage = new NavigationPage(new CommandsControlsDemo());
        //MainPage = new NavigationPage(new InputControlsDemo());
        //MainPage = new NavigationPage(new TextControlsDemo());
        //MainPage = new NavigationPage(new ActivityControlsDemo());
        //MainPage = new NavigationPage(new CollectionsControlsDemo());
        //MainPage = new BindigPage();
        //MainPage = new SliderPage();
        //MainPage = new BindingModes();
        MainPage = new BindigPage();
    }
}
